let a: number = 4;
let b: string = "Hello TS";
let c = true;

// Array
let arr: string[] = ["kalli", "linux"];

// any
let e: any = 3;
e = "three";

// function
function test(i: string): number | string {
  return "";
}

const test2 = (i: number): void => {
  return;
};

function printId(id: number | string) {
  if (typeof id === "number") {
    console.log(id.toString());
  } else {
    id.toUpperCase();
  }
}

arr = arr.map((x: string) => x.toLowerCase());

// Interface && type
type stringOrNumber = string | number;
interface AltPoint {
  x: number;
  y: number;
}

type Point = {
  x: number;
  y: number;
};

interface I3DPoint extends AltPoint {
  z: string;
}

const i = (point: AltPoint) => {
  const d: I3DPoint = point as I3DPoint;
};

// Get Json
interface Reply {
  userId: number;
  id: number;
  title: string;
  info: Info;
  tags: Tags;
}

interface Info {
  desc: string;
  isActive: boolean;
}

interface Tags {
  name: string;
}

type actionType = "up" | "down";

function performAction(action: actionType): -1 | 1 {
  switch (action) {
    case "down":
      return -1;
    case "up":
      return 1;
  }
}

// Class

class Aoint {
  private x: number;
  y!: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
}

const aoint = new Aoint(0, 0);
